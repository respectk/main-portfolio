import anime from 'animejs'

export function translateSkills(el) {
    anime({
        targets: el,
        translateX: 220,
        delay: anime.stagger(300),
        opacity: 1
    });

}
export function translateHeaderMyProjects(el) {
    anime({
        targets: el,
        translateX: 400,
        opacity: 1
    });
}
export function translateMyProects(el) {
    anime({
        targets: el,
        translateY: -20,
        delay: anime.stagger(300),
        opacity: 1
    });
}